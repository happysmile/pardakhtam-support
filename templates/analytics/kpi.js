var driverTransactionChart = function(response, mobile){
	dates = [];
	credits =[];
	response = JSON.parse( response);
	for( i=0 ; i<response.length; i++){
		day = response[i];
		dates.push( persianDate( new Date(day.date)).format('dddd-D MMMM'));

		var hadTransaction = false;
		for( j =0; j< day.list.length; j++){
			driver = day.list[j];
			if( driver.mobile === mobile){
				credits.push(driver.credit);
				hadTransaction = true;
			}
		}
		if( !hadTransaction)
			credits.push(0);
	}

	var canvas = document.getElementById('driverTransactionChart');
	new Chart(canvas,{
		type:'line',
		data:{
			labels:dates,
			datasets:[{
				data:credits
			}]
		},
		options:{
			responsive:true,
			maintainAspectRatio:true
		}

	});
}
var transactionCapacityPerDay = function( response){
	dates = [];
	credits =[];
	response = JSON.parse( response);
	for( i=0 ; i<response.length; i++){
		day = response[i];
		dates.push( persianDate( new Date(day.date)).format('dddd-D MMMM'));

		dayCredit = 0;
		for( j =0; j< day.list.length; j++)
			dayCredit+= day.list[j].credit;
		credits.push(dayCredit);
	}

	var canvas = document.getElementById('transactionCapacityPerDay');
	new Chart(canvas,{
		type:'line',
		data:{
			labels:dates,
			datasets:[{
				label:'حجم تراکنش / روز',
				data:credits
			}]
		},
		options:{
			responsive:true,
			maintainAspectRatio:true
		}

	});
}
var makeRequest = function(method, url, drawChart){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function(){
		if( this.readyState === XMLHttpRequest.DONE && this.status === 200){
			driverPerDay(this.responseText);
		}
	}
	xhttp.open( method, url , true);
	xhttp.send();
}


var driverPerDay = function( response){
    var response= JSON.parse( response)['res'];
	var date = persianDate.startOf('year');

	var drawDate = []
	var drawNum = []
	for(date;date<persianDate.now();date.add('days',1)){
		drawDate.append(date.format('YYYY-M-D'))
		if(response[date.format('YYYY-M-D')]){
			drawNum.append(response[date.format('YYYY-M-D')])
		}
		else
			drawNum.append(0)
	}

    var canvas = document.getElementById('drivers_per_day');
    new Chart(canvas,{
        type:'line',
        data:{
            labels:drawDate,
            datasets:[{
                label:'تعداد راننده کارکرده / روز',
                data:drawNum
            }]
        },
        options:{
            responsive:true,
            maintainAspectRatio:true
        }

    });
}

makeRequest('GET','/analytics/kpi/drivers_par_day',driverPerDay)