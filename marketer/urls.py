from django.conf.urls import url

from marketer import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'test', views.test),
    url(r'registered_driver', views.registered_driver),
    url(r'marketer_monitor_chart$', views.marketer_monitor_chart),
    url(r'line_performance$', views.line_performance),
    url(r'drivers_of_line$', views.drivers_of_line),
    url(r'lines_owner$', views.lines_owner),

    url(r'marketer_monitor_chart_tmp', views.marketer_monitor_chart_tmp),
    url(r'marketer_recent_monitor_chart_tmp', views.marketer_recent_monitor_chart_tmp),
    url(r'line_performance_tmp', views.line_performance_tmp),
    url(r'drivers_of_line_tmp', views.drivers_of_line_tmp)
]
