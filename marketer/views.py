# coding=utf-8
import json
from datetime import datetime, timedelta

from django.http import JsonResponse
# Create your views here.
from django.shortcuts import render
from khayyam import JalaliDatetime

from _date._Date import _Date
from analytics.models import Driver
from analytics.views import get_last_week_range
from db.mongo import DB
from marketer.models import Team


def test(request):
    print 'date is :', _Date.start_of_year('1397').get_date().strftime('%y-%m-%d')


def registered_driver(request):
    '''
    Used to get this marketer registered drivers.

    :param request:
        team_name   : name of team
        from_date   : from date
        to_date     : end date
    :return:
        a list of drivers, full detail
    '''
    if request.method == 'POST':
        team_name = request.POST.get('team_name', None)
        from_date = request.POST.get('from_date', None)
        to_date = request.POST.get('to_date', None)

        ## debug
        lwr = get_last_week_range()
        print lwr[0].todatetime().strftime("%s"), ':', lwr[1].todatetime().strftime("%s")
        ## end

        if not team_name or not from_date or not to_date:
            return JsonResponse({'msg': 'bad input'}, status=401, content_type='application/json')

        from_date = datetime.fromtimestamp(float(from_date))
        to_date = datetime.fromtimestamp(float(to_date))

        team = Team()
        if not team.team_exist(team_name):
            return JsonResponse({'msg': 'team name not exist'}, status=402, content_type='application/json')
        lines = team.get_team_lines(team_name)

        driver = Driver()
        drivers = driver.get_lines_registered_drivers(lines, from_date, to_date)
        return JsonResponse({'drivers': drivers}, status=200, content_type='application/json')
    else:
        return JsonResponse({'msg': 'just POST'}, status=403, content_type='application/json')


def marketer_monitor_chart(request):
    if request.method == 'POST':
        team = Team()
        teams_with_drivers = team.insert_drivers()

        for team_name, team in teams_with_drivers.iteritems():
            team['pays_num'] = []

        pays = DB.get_pays()

        start = _Date.start_of_year('1396')
        end = _Date.start_of_year('1396').month_after(1)

        now = JalaliDatetime.now()
        month_num = 1
        labels = []
        while start.get_date().todatetime() < now.todatetime():
            for team_name, team in teams_with_drivers.iteritems():
                team['pays_num'].append(pays.find({'receiver_mobile': {'$in': team['drivers']},
                                                   "date": {"$gt": start.get_date().todatetime(),
                                                            "$lt": end.get_date().todatetime()}}).count())

            labels.append(start.get_date().strftime("%B"))

            month_num += 1
            start.month_after(1)
            end.month_after(1)

        datasets = []
        for team_name, team in teams_with_drivers.iteritems():
            print team_name
            datasets.append({
                'label':team_name,
                'data':team['pays_num'],
                'backgroundColor': ['0', '0', '0', '0'],
                'borderColor': team['chart_color']
            })
        return JsonResponse({'datasets': datasets, 'labels': labels}, status=200, content_type='application/json')
    else:
        return JsonResponse({'msg':'just post'}, status=400, content_type='application/json')


def line_performance(request):
    if request.method == 'POST':
        line = request.POST.get('line', '649')
        start = JalaliDatetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        end = JalaliDatetime.now().replace(hour=0, minute=0, second=0, microsecond=0) + timedelta(days=1)

        labels = []
        data = []

        for i in range(0, 10):
            pipeline = [
                {"$match": {"date": {"$lt": end.todatetime(), "$gt": start.todatetime()}}},
                {"$lookup": {"from": "users", "localField": "receiver_mobile", "foreignField": "mobile",
                             "as": "user"}},
                {"$unwind": "$user"},
                {"$match": {"user.line": line, "user.role": "receiver"}}
            ]
            data.append(len(list(DB.get_pays().aggregate(pipeline))))
            labels.append(start.strftime('%A'))

            start -= timedelta(days=1)
            end -= timedelta(days=1)

        datasets = [
            {'data': data[::-1],
             'label': u'کارکرد خط ' + line,
             'backgroundColor': ['0', '0', '0', '0'],
             'borderColor': ['230', '74', '25', '0.8']
             }
        ]
        return JsonResponse({'datasets': datasets, 'labels': labels[::-1]}, status=200, content_type='application/json')
    else:
        return JsonResponse({'msg': 'just post'}, status=400, content_type='application/json')


def drivers_of_line(request):
    if request.method == 'POST':
        print ' i am post'
        line = request.POST.get('line', '649')

        start = _Date(JalaliDatetime.now()).end_of_day()
        end = _Date(JalaliDatetime.now()).start_of_day().days_behind(10)

        pipeline = [
            {"$match": {"date": {"$gt": end.to_datetime(), "$lt": start.to_datetime()}}},
            {"$lookup": {"from": "users", "localField": "receiver_mobile", "foreignField": "mobile",
                         "as": "user"}},
            {"$unwind": "$user"},
            {"$match": {"user.line": line}},
            {"$group": {"_id": "$receiver_mobile", "count": {"$sum": 1}, "driver": {"$push": "$$ROOT"}}},
            {"$sort": {"count": -1}},
        ]
        results = DB.get_pays().aggregate(pipeline)

        out = []
        for result in results:
            out.append({
                'count': result.get('count', -1),
                'name': result['driver'][0]['user']['name'],
                'family': result['driver'][0]['user']['family'],
                'mobile': result['driver'][0]['user']['mobile'],
                'type': result['driver'][0]['user']['type'],
            })
        return JsonResponse({'drivers': out}, status=200, content_type='application/json')
    else:
        return JsonResponse({'msg': 'just post'}, status=400, content_type='application/json')


def lines_owner(request):
    return JsonResponse({'lines_owners': Team().get_lines()}, status=200, content_type='application/json')


# temp code, mohammad davood will make it complete
def marketer_monitor_chart_tmp(request):
    team = Team()
    teams_with_drivers = team.insert_drivers()

    for team_name, team in teams_with_drivers.iteritems():
        team['pays_num'] = []

    pays = DB.get_pays()

    start = _Date.start_of_year('1396')
    end = _Date.start_of_year('1396').month_after(1)

    now = JalaliDatetime.now()
    month_num = 1
    labels = []
    while start.get_date().todatetime() < now.todatetime():
        for team_name, team in teams_with_drivers.iteritems():
            team['pays_num'].append(pays.find({'receiver_mobile': {'$in': team['drivers']},
                                               "date": {"$gt": start.get_date().todatetime(),
                                                        "$lt": end.get_date().todatetime()}}).count())

        labels.append(start.get_date().strftime("%B"))

        month_num += 1
        start.month_after(1)
        end.month_after(1)

    datasets = []
    for team_name, team in teams_with_drivers.iteritems():
        datasets.append({
            'label': team_name,
            'data': team['pays_num'],
            'backgroundColor': ['0', '0', '0', '0'],
            'borderColor': team['chart_color']
        })
    return render(request, 'line_chart.html', {'labels': json.dumps(labels), 'datasets': datasets})


def marketer_recent_monitor_chart_tmp(request):
    if request.method == 'GET':
        start = _Date(JalaliDatetime.strptime(request.GET.get('start', '1396/01/01'), '%Y/%m/%d'))
        end = _Date(JalaliDatetime.strptime(request.GET.get('end', '1396/01/02'), '%Y/%m/%d'))

        team = Team()
        teams_with_drivers = team.insert_drivers()

        for team_name, team in teams_with_drivers.iteritems():
            team['pays_num'] = []

        pays = DB.get_pays()

        now = JalaliDatetime.now()
        month_num = 1
        labels = []
        while start.get_date().todatetime() < now.todatetime():
            for team_name, team in teams_with_drivers.iteritems():
                team['pays_num'].append(pays.find({'receiver_mobile': {'$in': team['drivers']},
                                                   "date": {"$gt": start.get_date().todatetime(),
                                                            "$lt": end.get_date().todatetime()}}).count())

            labels.append(start.get_date().strftime("%A"))

            month_num += 1
            start.days_after(1)
            end.days_after(1)

        datasets = []
        for team_name, team in teams_with_drivers.iteritems():
            datasets.append({
                'label': team_name,
                'data': team['pays_num'],
                'backgroundColor': ['0', '0', '0', '0'],
                'borderColor': team['chart_color']
            })
        return render(request, 'line_chart.html', {'labels': json.dumps(labels), 'datasets': datasets})


def line_performance_tmp(request):
    if request.method == 'POST':
        line = request.POST.get('line', '649')
        start = JalaliDatetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        end = JalaliDatetime.now().replace(hour=0, minute=0, second=0, microsecond=0) + timedelta(days=1)

        labels = []
        data = []

        for i in range(0, 10):
            pipeline = [
                {"$match": {"date": {"$lt": end.todatetime(), "$gt": start.todatetime()}}},
                {"$lookup": {"from": "users", "localField": "receiver_mobile", "foreignField": "mobile", "as": "user"}},
                {"$unwind": "$user"},
                {"$match": {"user.line": line, "user.role": "receiver"}}
            ]
            data.append(len(list(DB.get_pays().aggregate(pipeline))))
            labels.append(start.strftime('%A'))

            start -= timedelta(days=1)
            end -= timedelta(days=1)

        datasets = [
            {'data': data[::-1],
             'label': u'کارکرد خط ' + line,
             'backgroundColor': ['0', '0', '0', '0'],
             'borderColor': ['230', '74', '25', '0.8']
             }
        ]
        return render(request, 'line_chart.html', {'labels': json.dumps(labels[::-1]), 'datasets': datasets})
    else:
        return render(request, 'marketer/line_performance.html')


def drivers_of_line_tmp(request):
    if request.method == 'POST':
        print ' i am post'
        line = request.POST.get('line', '649')

        start = _Date(JalaliDatetime.now()).end_of_day()
        end = _Date(JalaliDatetime.now()).start_of_day().days_behind(10)

        pipeline = [
            {"$match": {"date": {"$gt": end.to_datetime(), "$lt": start.to_datetime()}}},
            {"$lookup": {"from": "users", "localField": "receiver_mobile", "foreignField": "mobile", "as": "user"}},
            {"$unwind": "$user"},
            {"$match": {"user.line": line}},
            {"$group": {"_id": "$receiver_mobile", "count": {"$sum": 1}, "driver": {"$push": "$$ROOT"}}},
            {"$sort": {"count": -1}},
        ]
        results = DB.get_pays().aggregate(pipeline)

        out = []
        for result in results:
            out.append({
                'count': result.get('count', -1),
                'name': result['driver'][0]['user']['name'],
                'family': result['driver'][0]['user']['family'],
                'mobile': result['driver'][0]['user']['mobile'],
                'type': result['driver'][0]['user']['type'],
            })

        print out[0]
        return render(request, 'marketer/drivers_of_line.html', {'drivers': out})
    else:
        return render(request, 'marketer/drivers_of_line.html')


def lines_owner_tmp(request):
    return render(request, 'marketer/lines_owner.html', {'teams': Team().get_lines()})


def index(request):
    if request.method == 'GET':
        return render(request, 'marketer/index.html')
    elif request.method == 'POST':
        value = request.POST.get('action', False)
        if value == u'صاحب خط ها':
            return lines_owner_tmp(request)
        elif value == u'مسابقه':
            return marketer_monitor_chart_tmp(request)
        elif value == u'عملکرد خط':
            return render(request, 'marketer/line_performance.html')
        elif value == u'لیست رانندگان خط':
            return render(request, 'marketer/drivers_of_line.html')
