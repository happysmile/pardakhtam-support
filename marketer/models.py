# coding=utf-8


from __future__ import unicode_literals

# Create your models here.
from khayyam import JalaliDatetime

from analytics.models import Driver
from analytics.views import START_OF_FIRST_WEEK_OF_1396


class Team:
    def __init__(self):
        self.teams = {
            u'حجت': {
                'lines': ['258', '259', '556'],
                'chart_color': ['76', '175', '80', '1']
            },
            u'محمدداوود': {
                'lines':['183','144','324','277'],
                'chart_color':['230', '74', '25', '1']
            },
            u'میلاد': {
                'lines':['241'],
                'chart_color': ['130', '74', '25', '1']
            },
            u'محمدعلی': {
                'lines': ['585'],
                'chart_color': ['75', '0', '130', '1']
            },
            u'تینا': {
                'lines': ['516'],
                'chart_color': ['0', '128', '128', '1']
            }
        }

    def team_exist(self, team_name):
        return self.teams.get(team_name, None)

    def get_team(self, team_name):
        return self.teams.get(team_name, None)

    def get_team_lines(self, team_name):
        return self.teams.get(team_name).get('lines')

    def get_lines(self):
        out = []
        for key,value in self.teams.iteritems():
            out.append((key,value.get('lines')))
        return out

    def insert_drivers(self):
        driver = Driver()
        for team_name, team in self.teams.iteritems():
            drivers = driver.get_lines_registered_drivers(team['lines'],
                                                          JalaliDatetime(*START_OF_FIRST_WEEK_OF_1396).todatetime(),
                                                          JalaliDatetime.now().todatetime())
            self.teams[team_name]['drivers'] = [d['mobile'] for d in drivers]
        return self.teams