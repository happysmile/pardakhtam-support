from datetime import timedelta

from khayyam import JalaliDatetime


class _Date:
    '''
    date must be JalaiDatetime
    '''

    def __init__(self, date):
        self.date = date

    @staticmethod
    def start_of_year(year):
        return _Date(JalaliDatetime(year=year, month=1, day=1, hour=0, minute=0, second=0, microsecond=0))

    def start_of_day(self):
        self.date = self.date.replace(hour=0, minute=0, second=0, microsecond=0)
        return self

    def end_of_day(self):
        return self.start_of_day().tomorrow()

    def tomorrow(self):
        self.date = self.date + timedelta(days=1)
        return self

    def month_after(self, month):
        if self.date.month + month < 13:
            self.date = self.date.replace(month=self.date.month + month)
        return self

    def month_before(self, month):
        self.date = self.date.replace(month=self.date.month - month)
        return self

    def start_of_month(self):
        self.date.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        return self

    def week_after(self, week):
        return self.days_after(week*7)

    def days_behind(self, days):
        self.date = self.date - timedelta(days=days)

        return self

    def days_after(self, days):
        self.date = self.date + timedelta(days=days)

        return self

    def get_date(self):
        return self.date

    def to_datetime(self):
        return self.date.todatetime()
