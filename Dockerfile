FROM python:2.7


WORKDIR /usr/src/app
COPY . .

RUN pip install -r ./requirements.txt

RUN chmod -R ug+rwx . && chown -R 1001:0 .
USER 1001

ENV DB_USER 'userUDC'
ENV DB_PASS '3PCe3kG8wQpv8trD'
ENV DB_SERVER 'mongodb'
ENV DB_NAME 'pardakhtam'

ENV PARDAKHTAM_SUPPORT_DEBUG 'False'


EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
