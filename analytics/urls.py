from django.conf.urls import url

from analytics import views
urlpatterns = [
    url(r'registered_driver_per_week', views.registered_driver_per_week),
    url(r'drivers_num_per_week', views.drivers_num_per_week),
    url( r'registered_passenger_per_month', views.registered_passenger_per_month),
    url(r'drivers_list', views.drivers_list),
    url(r'last_week_active_drivers', views.last_week_active_drivers),
    url(r'drivers_list', views.drivers_list),
    url(r'last_week_active_lines', views.last_week_active_lines),
    url(r'driver_registration_monitor', views.driver_registration_monitor),
    url(r'register_to_active_monitor_chart', views.register_to_active_monitor_chart),
    url(r'register_to_active_monitor_list', views.register_to_active_monitor_list),
    url(r'active_to_active_monitor_chart', views.active_to_active_monitor_chart),
    url(r'active_to_active_monitor_list', views.active_to_active_monitor_list),
    url(r'driver_activation_monitor', views.driver_activation_monitor),
    url(r'charge_amount_per_month', views.charge_amount_per_month),
    url(r'charge_num_per_month', views.charge_num_per_month),
    url(r'left_credit_per_month', views.left_credit_per_month),
    url(r'revenue_per_month', views.revenue_per_month),
    url(r'transaction_per_month', views.transaction_per_month),
    url(r'active_passengers_per_month', views.active_passengers_per_month),
    url(r'active_drivers_per_month', views.active_drivers_per_month),
    url(r'passenger_retention_curve', views.passenger_retention_curve),
    url(r'driver_retention_curve', views.driver_retention_curve),

    url(r'test', views.test)
]

