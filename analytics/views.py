# coding=utf-8

import json
from datetime import timedelta

import django_excel
from django.http import JsonResponse
from django.shortcuts import render
from khayyam import JalaliDatetime

from _date._Date import _Date
from analytics.models import Driver, Charge, Pay, calculate_bank_revenue_per_month, Passenger
from db.mongo import DB
from line import line

START_OF_FIRST_WEEK_OF_1396 = [1395, 12, 28, 0, 0, 0, 0]
END_OF_FIRST_WEEK_OF_1396 = [1396, 1, 4, 0, 0, 0, 0]


def registered_driver_per_week(request):
    users = DB.get_users()

    start_of_week = JalaliDatetime(1395, 12, 28, 0, 0, 0, 0)
    end_of_week = JalaliDatetime(1396, 1, 4, 0, 0, 0, 0)
    week_num = 1
    now = JalaliDatetime.now()

    needed_driver_num = 10

    labels = []

    registered_driver_num_label = 'راننده ثبت نام شده / هفته'
    registered_driver_num_list = []
    needed_driver_num_label = 'راننده ی مورد نیاز / هفته'
    needed_driver_num_list = []
    while start_of_week < now:
        num = users.find({'role': 'receiver',
                          'registerDate': {'$gt': start_of_week.todatetime(), '$lt': end_of_week.todatetime()},
                          'activated': 'yes'}).count()

        registered_driver_num_list.append(num)
        needed_driver_num_list.append(needed_driver_num)
        labels.append(week_num)

        start_of_week += timedelta(weeks=1)
        end_of_week += timedelta(weeks=1)
        week_num += 1
        needed_driver_num += needed_driver_num * 0.07
    return render(request, 'analytics/chart.html', {'labels': labels,
                                                    'registered_driver_num_label': registered_driver_num_label,
                                                    'registered_driver_num_list': registered_driver_num_list,
                                                    'needed_driver_num_label': needed_driver_num_label,
                                                    'needed_driver_num_list': needed_driver_num_list})


def drivers_num_per_week(request):
    users = DB.get_users()
    pays = DB.get_pays()

    start_of_week = JalaliDatetime(1395, 12, 28, 0, 0, 0, 0)
    end_of_week = JalaliDatetime(1396, 1, 4, 0, 0, 0, 0)

    now = JalaliDatetime.now()

    week_num = 1

    labels = []
    drivers_num_list = []
    worked_drivers_num_list = []
    drivers_num_label = 'تعداد راننده / هفته'
    worked_drivers_num_label = 'تعداد راننده کارکرده / هفته'
    while start_of_week < now:
        drivers_num = users.find({'role': 'receiver',
                                  'registerDate': {'$lt': start_of_week.todatetime()},
                                  'activated': 'yes'}).count()

        worked_drivers_num = len(
            pays.find({'date': {'$gt': start_of_week.todatetime(), '$lt': end_of_week.todatetime()},
                       'amount': {'$gt': 1000}}).distinct('receiver_id'))

        drivers_num_list.append(drivers_num)
        worked_drivers_num_list.append(worked_drivers_num)

        labels.append(week_num)

        week_num += 1
        start_of_week += timedelta(weeks=1)
        end_of_week += timedelta(weeks=1)

    return render(request, 'analytics/drivers_num_per_week.html', {'labels': labels,
                                                                   'drivers_num_label': drivers_num_label,
                                                                   'drivers_num_list': drivers_num_list,
                                                                   'worked_drivers_num_label': worked_drivers_num_label,
                                                                   'worked_drivers_num_list': worked_drivers_num_list})


def registered_passenger_per_month(request):
    users = DB.get_users()

    start_of_month = JalaliDatetime(1395, 7, 1, 0, 0, 0, 0)
    end_of_month = JalaliDatetime(1395, 7, 8, 0, 0, 0, 0)
    month_num = 1
    now = JalaliDatetime.now()

    labels = []

    registered_passenger_num_label = 'مسافر ثبت نام شده / هفته'
    registered_passenger_num_list = []

    while start_of_month < now:
        num = users.find({'role': 'payer',
                          'registerDate': {'$gt': start_of_month.todatetime(), '$lt': end_of_month.todatetime()},
                          'activated': 'yes'}).count()

        registered_passenger_num_list.append(num)

        labels.append(month_num)

        print num, month_num

        start_of_month += timedelta(weeks=1)
        end_of_month += timedelta(weeks=1)
        month_num += 1
    return render(request, 'analytics/chart.html', {'labels': labels,
                                                    'registered_driver_num_label': registered_passenger_num_label,
                                                    'registered_driver_num_list': registered_passenger_num_list})


def drivers_list(request):
    users = DB.get_users()

    drivers = users.find({'role': 'receiver'})
    out = []
    out.append(['name', 'family', 'mobile', 'old_line', 'new_line', 'info'])
    for driver in drivers:
        out.append([driver.get('name', ''), driver.get('family', ''), driver.get('mobile', ''), driver.get('line', '')])

    return django_excel.make_response_from_array(out, 'csv', 200)


def get_last_week_range():
    now = JalaliDatetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

    start = now - timedelta(days=(now.weekday() + 7))
    end = now - timedelta(days=now.weekday())

    return [start, end]


def get_this_week_range():
    now = JalaliDatetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

    start = now - timedelta(days=now.weekday())
    end = start + timedelta(days=7)

    return [start, end]


def last_week_active_drivers(request):
    pays = DB.get_pays()
    last_week = get_last_week_range()
    pipeline = [
        {"$match": {"date": {"$lt": last_week[1].todatetime(), "$gt": last_week[0].todatetime()},
                    'amount': {'$gt': 1000}}},
        {"$group": {"_id": "$receiver_mobile", "count": {"$sum": 1}}},
        {"$lookup": {"from": "users", "localField": "_id", "foreignField": "mobile", "as": "user"}},
        {"$unwind": "$user"},
        {"$group": {"_id": {"line": "$user.line", "type": "$user.type"}, "count": {"$max": "$count"},
                    "drivers": {"$push": "$$ROOT"}}},
        {"$project": {"count": 1,
                      "docs": {"$setDifference": [{"$map": {"input": "$drivers",
                                                            "as": "driver",
                                                            "in": {"$cond": [
                                                                {"$eq": ["$count", "$$driver.count"]},
                                                                "$$driver",
                                                                "false"]
                                                            }
                                                            }
                                                   },
                                                  ["false"]
                                                  ]
                               }
                      }},
        {"$unwind": "$docs"},
        {"$project": {
            "_id": 0,
            "name": "$docs.user.name",
            "family": "$docs.user.family",
            "mobile": "$docs.user.mobile",
            "count": "$docs.count",
            "line": "$docs.user.line",
            "type": "$docs.user.type"
        }},
        {"$sort": {"count": -1}}
    ]
    active_drivers = pays.aggregate(pipeline)

    out = []
    for active_driver in active_drivers:
        try:
            _line = line.lines[active_driver['line']]
            active_driver['line_name'] = _line['from'] + '-' + _line['to']
            active_driver['support_day'] = _line['day']
        except KeyError:
            active_driver['line_name'] = active_driver.get('line')
            active_driver['support_day'] = 0
        if len(active_driver.get('mobile', '')) > 0:
            out.append(active_driver)

    out = sorted(out, key=lambda value: value['support_day'])
    for active_driver in out:
        active_driver['support_day'] = line.days[active_driver['support_day']]['name']
    return render(request, 'analytics/last_week_active_drivers.html', {'active_drivers': out})


def last_week_active_lines(request):
    pays = DB.get_pays()

    last_week = get_last_week_range()

    pipeline = [
        {"$match": {"date": {"$lt": last_week[1].todatetime(), "$gt": last_week[0].todatetime()}}},
        {"$group": {"_id": "$receiver_mobile", "count": {"$sum": 1}}},
        {"$lookup": {"from": "users", "localField": "_id", "foreignField": "mobile", "as": "user"}},
        {"$unwind": "$user"},
        {"$group": {"_id": "$user.line", "driver_sum": {"$sum": 1}, "pay_num": {"$sum": "$count"}}},
        {"$sort": {"_id": 1}}
    ]
    active_lines = pays.aggregate(pipeline)

    out = []
    for active_line in active_lines:
        line = {}
        line['line'] = active_line.get("_id")
        line['driver_num'] = active_line.get("driver_sum")
        line['pay_num'] = active_line.get("pay_num")
        out.append(line)
    return render(request, 'analytics/last_week_active_lines.html', {"lines": out})


def test(request):
    driver = Driver()
    start = get_last_week_range()[0]
    end = get_last_week_range()[1]
    for d in driver.get_active_drivers_detailed(start.todatetime(), end.todatetime()):
        print d


# khat shekanan
def driver_registration_monitor(request):
    if request.method == 'GET':
        period = request.GET.get('period', 'weekly')
        if period == 'weekly':
            start = JalaliDatetime(*START_OF_FIRST_WEEK_OF_1396)
            end = JalaliDatetime(*END_OF_FIRST_WEEK_OF_1396)
            now = JalaliDatetime.now()

            driver = Driver()
            week_num = 1
            registration_target = []
            registered_driver = []
            labels = []
            while start < now:
                registered_driver.append(driver.get_registered_drivers(start.todatetime(), end.todatetime()).count())
                registration_target.append(driver.driver_registration_target_per_month[end.month - 1] / 4)
                # labels.append(u'هفته %dام %s' % (week_num % 4 + 1, start.monthname()))
                labels.append(week_num)
                week_num += 1
                start += timedelta(days=7)
                end += timedelta(days=7)

            datasets = [
                {'label': 'ثبت نام شده',
                 'data': registered_driver,
                 'backgroundColor': ['255', '87', '34', '0.8'],
                 'borderColor': ['230', '74', '25', '0.8']},
                {'label': 'هدف گزاری شده',
                 'data': registration_target,
                 'backgroundColor': ['76', '175', '80', '1'],
                 'borderColor': ['56', '142', '60', '1']}
            ]
            return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})


# sar bezanan
## register to active
def register_to_active_monitor_chart(request):
    if request.method == 'GET':
        period = request.GET.get('period', 'weekly')
        if period == 'weekly':
            start = JalaliDatetime(*START_OF_FIRST_WEEK_OF_1396)
            end = JalaliDatetime(*END_OF_FIRST_WEEK_OF_1396)
            now = JalaliDatetime.now()

            driver = Driver()
            week_num = 1
            num_newly_active_drivers = []
            num_registered_drivers = []
            labels = []
            while start < now:
                active_drivers = driver.get_active_drivers(start.todatetime(), end.todatetime())
                registered_drivers = driver.get_registered_drivers(start.todatetime(), end.todatetime())

                active_newly_registered = 0
                for registered_driver in registered_drivers:
                    if registered_driver.get('mobile') in active_drivers:
                        active_newly_registered += 1

                num_newly_active_drivers.append(active_newly_registered)
                num_registered_drivers.append(registered_drivers.count())
                labels.append(week_num)

                week_num += 1
                start += timedelta(days=7)
                end += timedelta(days=7)

            datasets = [
                {'label': 'فعال شدگان',
                 'data': num_newly_active_drivers,
                 'backgroundColor': ['255', '87', '34', '0.8'],
                 'borderColor': ['230', '74', '25', '0.8']},
                {'label': 'ثبت نام شدگان',
                 'data': num_registered_drivers,
                 'backgroundColor': ['76', '175', '80', '1'],
                 'borderColor': ['56', '142', '60', '1']}
            ]

            return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})


def register_to_active_monitor_list(request):
    if request.method == 'GET':
        period = request.GET.get('period', 'weekly')
        if period == 'weekly':
            start = get_this_week_range()[0]
            end = get_this_week_range()[1]

            driver = Driver()

            active_drivers = driver.get_active_drivers(start.todatetime(), end.todatetime())
            registered_drivers = driver.get_registered_drivers(start.todatetime(), end.todatetime())

            registered_drivers_ = []
            for registered_driver in list(registered_drivers):
                if registered_driver['mobile'] not in active_drivers:
                    registered_drivers_.append(registered_driver)
                    del registered_driver['_id']

            return JsonResponse({'registered_drivers': registered_drivers_}, status=200,
                                content_type='application/json')


## active to active
def active_to_active_monitor_chart(request):
    if request.method == 'GET':
        period = request.GET.get('period', 'weekly')
        if period == 'weekly':
            start = JalaliDatetime(*START_OF_FIRST_WEEK_OF_1396) - timedelta(days=7)
            end_week_one = JalaliDatetime(*START_OF_FIRST_WEEK_OF_1396)
            end_week_two = end_week_one + timedelta(days=7)
            now = JalaliDatetime.now()
            week_num = 1

            labels = []
            last_week_actives_num = []
            this_week_actives_num = []

            driver = Driver()
            while end_week_one < now:
                last_week_actives = driver.get_active_drivers_detailed(start.todatetime(), end_week_one.todatetime())
                this_week_actives = driver.get_active_drivers_detailed(end_week_one.todatetime(),
                                                                       end_week_two.todatetime())

                labels.append(week_num)
                last_week_actives_num.append(len(last_week_actives))

                this_week_actives_num_ = 0
                for old_active in last_week_actives:
                    for new_active in this_week_actives:
                        if old_active.get('mobile', 'old') == new_active.get('mobile', 'new'):
                            this_week_actives_num_ += 1
                            break
                this_week_actives_num.append(this_week_actives_num_)

                week_num += 1
                start += timedelta(days=7)
                end_week_one += timedelta(days=7)
                end_week_two += timedelta(days=7)

            datasets = [
                {'label': 'فعال مانده از هفته گذشته',
                 'data': this_week_actives_num,
                 'backgroundColor': ['255', '87', '34', '0.8'],
                 'borderColor': ['230', '74', '25', '0.8']},
                {'label': 'فعالان هفته گذشته',
                 'data': last_week_actives_num,
                 'backgroundColor': ['76', '175', '80', '1'],
                 'borderColor': ['56', '142', '60', '1']}
            ]
            return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})


def active_to_active_monitor_list(request):
    if request.method == 'GET':
        period = request.GET.get('period', 'weekly')
        if period == 'weekly':
            start = get_last_week_range()[0]
            end_week_one = start + timedelta(days=7)
            end_week_two = end_week_one + timedelta(days=7)

            driver = Driver()
            last_week_actives = driver.get_active_drivers_detailed(start.todatetime(), end_week_one.todatetime())
            this_week_actives = driver.get_active_drivers_detailed(end_week_one.todatetime(), end_week_two.todatetime())

            de_activated = []
            for old_active in last_week_actives:
                is_active = False
                for new_active in this_week_actives:
                    if old_active.get('mobile', 'old') == new_active.get('mobile', 'new'):
                        is_active = True
                        break
                if not is_active:
                    del old_active['_id']
                    de_activated.append(old_active)
            return JsonResponse({'de_active_drivers': de_activated}, status=200)


## de-active
def driver_activation_monitor(request):
    if request.method == 'GET':
        period = request.GET.get('period', 'weekly')
        if period == 'weekly':
            start = JalaliDatetime(*START_OF_FIRST_WEEK_OF_1396)
            end = JalaliDatetime(*END_OF_FIRST_WEEK_OF_1396)
            now = JalaliDatetime.now()

            week_num = 0
            active_driver_num = []
            target_active_driver_num = []
            labels = []
            driver = Driver()
            while start < now:
                active_driver_num.append(len(driver.get_active_drivers(start.todatetime(), end.todatetime())))
                target_active_driver_num.append(driver.driver_activation_target_per_month[end.month - 1])
                labels.append(week_num)

                week_num += 1
                start += timedelta(days=7)
                end += timedelta(days=7)

            datasets = [
                {'label': 'تعداد راننده ی فعال',
                 'data': active_driver_num,
                 'backgroundColor': ['255', '87', '34', '0.8'],
                 'borderColor': ['230', '74', '25', '0.8']},
                {'label': 'راننده فعال هدف گزاری شده',
                 'data': target_active_driver_num,
                 'backgroundColor': ['76', '175', '80', '1'],
                 'borderColor': ['56', '142', '60', '1']}
            ]
            return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})


#charge_amount/month
def charge_amount_per_month(request):
    charge = Charge()
    start = _Date.start_of_year(1396)
    end = _Date.start_of_year(1396).month_after(1)
    now = JalaliDatetime.now()

    labels = []
    charges_amount = []
    while start.get_date() < now:
        charges_amount.append(charge.get_sum_succ(start.to_datetime(), end.to_datetime()))
        labels.append(start.get_date().strftime('%m'))

        start.month_after(1)
        end.month_after(1)

    datasets = [
        {'label':u'مقدار شارژ بر حسب ماه',
         'data' :charges_amount,
         'backgroundColor': ['145', '180', '47', '0'],
         'borderColor': ['145', '180', '47', '1']}
    ]
    return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})

#charge_num/month
def charge_num_per_month(request):
    charge = Charge()
    start = _Date.start_of_year(1396)
    end = _Date.start_of_year(1396).month_after(1)
    now = JalaliDatetime.now()

    labels = []
    charges_amount = []
    while start.get_date() < now:
        charges_amount.append(charge.get_count_succ(start.to_datetime(), end.to_datetime()))
        labels.append(start.get_date().strftime('%m'))

        start.month_after(1)
        end.month_after(1)

    datasets = [
        {'label':u'تعداد شارژ بر حسب ماه',
         'data' :charges_amount,
         'backgroundColor': ['145', '180', '47', '0'],
         'borderColor': ['145', '180', '47', '1']}
    ]
    return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})

#left_credit/month
def left_credit_per_month(request):
    start = _Date.start_of_year(1396)
    end = _Date.start_of_year(1396).month_after(1)
    now = JalaliDatetime.now()

    charge = Charge()
    pay = Pay()

    labels = []
    left_credit = []
    while end.get_date() < now:
        labels.append(end.get_date().strftime('%m'))

        left_credit.append(charge.get_sum_succ(start.to_datetime(), end.to_datetime()) - pay.get_sum(start.to_datetime(), end.to_datetime()))

        end.month_after(1)


    datasets = [
        {'label':u'میزان اعتبار مانده در حساب',
         'data' :left_credit,
         'backgroundColor': ['145', '180', '47', '0'],
         'borderColor': ['145', '180', '47', '1']}
    ]
    return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})

#revenue/month
def revenue_per_month(request):
    start_of_year = _Date.start_of_year(1396).to_datetime()
    start = _Date.start_of_year(1396)
    end = _Date.start_of_year(1396).month_after(1)
    now = JalaliDatetime.now()

    charge = Charge()
    pay = Pay()

    labels = []
    total_revenue = []
    transaction_revenue = []
    bank_revenue = []
    while start.get_date() < now:
        labels.append(start.get_date().strftime('%m'))

        _start = start.to_datetime()
        _end = end.to_datetime()

        r1 = charge.calculate_revenue(_start, _end)

        left_credit = charge.get_sum_succ(start_of_year, _end) - pay.get_sum(start_of_year, _end)
        r2 = calculate_bank_revenue_per_month(left_credit)

        r1 = int(r1)
        r2 = int(r2)

        transaction_revenue.append(r1)
        bank_revenue.append(r2)
        total_revenue.append(r1+r2)

        end.month_after(1)
        start.month_after(1)

    datasets = [
        {
            'label':u'درآمد از رسوب پول',
            'data' :bank_revenue,
            'backgroundColor': ['145', '180', '47', '0'],
            'borderColor': ['76', '175', '80', '1']
         },
        {
            'label':u'درآمد از کارمزد شاپرک',
            'data' :transaction_revenue,
            'backgroundColor': ['145', '180', '47', '0'],
            'borderColor': ['230', '74', '25', '1']
         },
        {
            'label':u'کل درآمد',
            'data' :total_revenue,
            'backgroundColor': ['145', '180', '47', '0'],
            'borderColor': ['130', '74', '25', '1']
         },
    ]
    return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})

#transaction/month OR (pays/month)
def transaction_per_month(request):
    start = _Date.start_of_year(1396)
    end = _Date.start_of_year(1396).month_after(1)
    now = JalaliDatetime.now()

    pays = Pay()

    labels = []
    transaction_count = []
    while start.get_date() < now:
        labels.append(start.get_date().strftime('%m'))
        transaction_count.append(pays.get_count(start.to_datetime(), end.to_datetime()))

        start.month_after(1)
        end.month_after(1)

    datasets = [
        {'label':u'تعداد تراکنش',
         'data' :transaction_count,
         'backgroundColor': ['145', '180', '47', '0'],
         'borderColor': ['145', '180', '47', '1']}
    ]
    return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})

#active_passenger/month
def active_passengers_per_month(request):
    start = _Date.start_of_year(1396)
    end = _Date.start_of_year(1396).month_after(1)
    now = JalaliDatetime.now()

    passenger = Passenger()
    labels = []
    active_passengers = []
    old_active_passengers = []
    while start.get_date()<now:
        labels.append(start.get_date().strftime('%m'))

        active_passengers.append(passenger.get_active(start.to_datetime(), end.to_datetime()))
        old_active_passengers.append(passenger.get_old_active(start.to_datetime(), end.to_datetime()))

        start.month_after(1)
        end.month_after(1)

    datasets = [
        {
            'label':u'تعداد مسافر فعال',
         'data' :active_passengers,
         'backgroundColor': ['145', '180', '47', '0'],
         'borderColor': ['145', '180', '47', '1']},
        {
            'label':u'تعداد مسافر فعال قدیمی',
            'data' :old_active_passengers,
            'backgroundColor': ['145', '180', '47', '0'],
            'borderColor': ['230', '74', '25', '1']
         }
    ]
    return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})

#active_drivers/month
def active_drivers_per_month(request):
    start = _Date.start_of_year(1396)
    end = _Date.start_of_year(1396).month_after(1)
    now = JalaliDatetime.now()

    driver = Driver()
    labels = []
    active_drivers = []
    old_active_drivers = []
    while start.get_date()<now:
        labels.append(start.get_date().strftime('%m'))

        active_drivers.append(len(driver.get_active_drivers(start.to_datetime(), end.to_datetime())))
        old_active_drivers.append(driver.get_old_active_drivers(start.to_datetime(), end.to_datetime()))

        start.month_after(1)
        end.month_after(1)

    datasets = [
        {
            'label':u'تعداد راننده فعال',
         'data' :active_drivers,
         'backgroundColor': ['145', '180', '47', '0'],
         'borderColor': ['145', '180', '47', '1']},
        {
            'label':u'تعداد راننده فعال قدیمی',
            'data' :old_active_drivers,
            'backgroundColor': ['145', '180', '47', '0'],
            'borderColor': ['230', '74', '25', '1']
         }
    ]
    return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})

#passenger retention curve
def passenger_retention_curve(request):
    start = _Date.start_of_year(1396)
    end = _Date.start_of_year(1396).month_after(1)
    now = JalaliDatetime.now()

    passenger = Passenger()
    retentions = []
    labels = []
    while start.get_date()<now:
        retentions.append(passenger.calculate_retention(start.to_datetime(), end.to_datetime()))
        labels.append(start.get_date().strftime('%m'))

        start.month_after(1)
        end.month_after(1)

    datasets = []
    for i in range(0, len(retentions)):
        datasets.append(
        {
            'label':u'نرخ بازگشت %dام' % i,
            'data' :retentions[i],
            'backgroundColor': ['145', '180', '47', '0'],
            'borderColor': ['230', '74', '25', str(0.2+ ((1-0.2)/len(retentions))*i)]
         }
        )
    return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})


#driver retention curve
def driver_retention_curve(request):
    start = _Date.start_of_year(1396)
    end = _Date.start_of_year(1396).month_after(1)
    now = JalaliDatetime.now()

    driver = Driver()
    retentions = []
    labels = []
    while start.get_date()<now:
        retentions.append(driver.calculate_retention(start.to_datetime(), end.to_datetime()))
        labels.append(start.get_date().strftime('%m'))

        start.month_after(1)
        end.month_after(1)

    datasets = []
    for i in range(0, len(retentions)):
        datasets.append(
        {
            'label':u'نرخ بازگشت %dام' % i,
            'data' :retentions[i],
            'backgroundColor': ['145', '180', '47', '0'],
            'borderColor': ['230', '74', '25', str(0.2+ ((1-0.2)/len(retentions))*i)]
         }
        )
    return render(request, 'line_chart.html', {'labels': labels, 'datasets': datasets})