from __future__ import unicode_literals

from khayyam import JalaliDatetime

from _date._Date import _Date
from db.mongo import DB


class Driver:
    def __init__(self):
        # starting from shahrivar
        self.driver_registration_target_per_month = [33, 41, 30, 60, 106, 563, 900, 1200, 2000, 2000, 2250, 2250, 2500,
                                                     3000, 3000, 3000, 3000]

        self.driver_activation_target_per_month = [44, 87, 110, 135, 110, 419, 810, 1327, 2262, 3009, 3870, 4752, 5664,
                                                   6765, 7850, 8772, 9557]

    def get_active_drivers(self, start, end):
        pipeline = [
            {"$match": {"date": {"$gt": start, "$lt": end}, "amount": {"$gt": 1000}}},
            {"$group": {"_id": "$receiver_mobile"}}
        ]
        return [d['_id'] for d in DB.get_pays().aggregate(pipeline)]

    def get_old_active_drivers(self, start, end):
        pays = DB.get_pays()
        pipeline = [
            {'$match':{'date':{'$gt':start, '$lt':end}}},
            {'$lookup':{ 'from':'users', 'localField':'receiver_id', 'foreignField':'_id', 'as':'driver'}},
            {'$unwind':'$driver'},
            {'$match': {'driver.registerDate': {'$lt': start}}},
            {'$group':{'_id':None, 'active_drivers':{'$addToSet':'$receiver_id'}}}
        ]

        return len(pays.aggregate(pipeline).next()['active_drivers'])

    def get_active_drivers_detailed(self, start, end):
        pipeline = [
            {"$match": {"date": {"$gt": start, "$lt": end}, "amount": {"$gt": 1000}}},
            {"$group": {"_id": "$receiver_mobile", "max_amount": {"$max": "$amount"}}},
            {"$lookuplookup": {"from": "users", "localField": "_id", "foreignField": "mobile", "as": "user"}},
            {"$match": {"$or": [{"user.activated": "yes"}, {"user.telegramActivated": "yes"}]}}
        ]
        return [d['user'][0] for d in DB.get_pays().aggregate(pipeline)]

    def get_registered_drivers(self, start, end):
        users = DB.get_users()
        users.find()
        return users.find({'role': 'receiver',
                           "$or": [{"activated": "yes"}, {"telegramActivated": "yes"}],
                           'line': {'$ne': 'marketing'},
                           'registerDate': {'$gt': start, '$lt': end}})
    def get_registered_ids(self, start, end):
        return [ driver['_id'] for driver in self.get_registered_drivers(start, end)]

    def get_lines_registered_drivers(self, lines, start, end):
        users = DB.get_users()
        users = users.find({'role': 'receiver',
                            "$or": [{"activated": "yes"}, {"telegramActivated": "yes"}],
                            'line': {'$in': lines},
                            'registerDate': {'$gt': start, '$lt': end}})
        users_out = []
        for user in users:
            del user['_id']
            users_out.append(user)

        return users_out

    #gives array of retention percent for the cohort of registered users in [start,end] period
    #start and end are instance of _Date
    def calculate_retention(self, start, end):
        start = _Date(JalaliDatetime(start))
        end = _Date(JalaliDatetime(end))
        now = JalaliDatetime.now()

        pay = Pay()
        registered_drivers = self.get_registered_ids(start.to_datetime(), end.to_datetime())

        retention = []
        retention.append(100)
        while True:
            start.month_after(1)
            end.month_after(1)

            if start.get_date() < now:
                retained = pay.how_many_have_receives(registered_drivers, start.to_datetime(), end.to_datetime())
                retention.append(float(retained) / len(registered_drivers) * 100)
            else:
                break

        return retention

class Charge:
    def get_succ(self, start, end):
        charges = DB.get_charges()
        return charges.find({'date':{'$gt':start, '$lt':end},
                                'refNum':{'$exists':True}})

    def get_sum_succ(self, start, end):
        charges = DB.get_charges()
        pipeline = [
            {'$match':{'date':{'$gt':start, '$lt':end}, 'refNum':{'$exists':True}}},
            {'$group':{'_id':None, 'sum':{'$sum':'$amount'}}}
        ]

        return charges.aggregate(pipeline).next()['sum']

    def get_count_succ(self, start, end):
        charges = DB.get_charges()
        pipeline = [
            {'$match':{'date':{'$gt':start, '$lt':end}, 'refNum':{'$exists':True}}},
            {'$group':{'_id':None, 'sum':{'$sum':1}}}
        ]

        return charges.aggregate(pipeline).next()['sum']

    def calculate_transaction_revenue(self, amount):
        revenue = amount*0.01
        if revenue > 250: revenue = 250

        #shaparak
        revenue -= 25

        #maliat
        revenue -= (revenue*0.009)

        #sahm e ma az Iran Kish
        revenue = revenue*0.85
        return revenue

    def calculate_revenue(self, start, end):
        charges = self.get_succ(start, end)

        sum = 0.0
        for charge in charges:
            sum += self.calculate_transaction_revenue(charge['amount'])

        return sum

class Pay:
    def get_sum(self, start, end):
        pays = DB.get_pays()
        pipeline = [
            {'$match':{'date':{'$gt':start, '$lt':end}}},
            {'$group':{'_id':None, 'sum':{'$sum':'$amount'}}}
        ]
        return pays.aggregate(pipeline).next()['sum']

    def get_count(self, start, end):
        pays = DB.get_pays()
        pipeline = [
            {'$match':{'date':{'$gt':start, '$lt':end}}},
            {'$group':{'_id':None, 'count':{'$sum':1}}}
        ]
        return pays.aggregate(pipeline).next()['count']

    #DSC: how many of given passengers have pays in given period
    #RETURN: int
    def how_many_have_pay(self, users_id, start, end):
        pays = DB.get_pays()
        pipeline = [
            {'$match':{'payer_id':{'$in':users_id}, 'date':{'$gt':start, '$lt':end}}},
            {'$group':{'_id':None, 'retained_users':{'$addToSet':'$payer_id'}}}
        ]

        return len(pays.aggregate(pipeline).next()['retained_users'])

    #DSC: how many of given drivers have receives in given period
    #RETURN: int
    def how_many_have_receives(self, users_id, start, end):
        pays = DB.get_pays()
        pipeline = [
            {'$match':{'receiver_id':{'$in':users_id}, 'date':{'$gt':start, '$lt':end}}},
            {'$group':{'_id':None, 'retained_users':{'$addToSet':'$receiver_id'}}}
        ]

        return len(pays.aggregate(pipeline).next()['retained_users'])

class Passenger:
    def get_active(self, start, end):
        pays = DB.get_pays()
        pipeline = [
            {'$match':{'date':{'$gt':start, '$lt':end}}},
            {'$group':{'_id':None, 'active_passengers':{'$addToSet':'$payer_id'}}}
        ]
        return len(pays.aggregate(pipeline).next()['active_passengers'])

    def get_old_active(self, start, end):
        pays = DB.get_pays()
        pipeline = [
            {'$match':{'date':{'$gt':start, '$lt':end}}},
            {'$lookup':{ 'from':'users', 'localField':'payer_id', 'foreignField':'_id', 'as':'passenger'}},
            {'$unwind':'$passenger'},
            {'$match': {'passenger.registerDate': {'$lt': start}}},
            {'$group':{'_id':None, 'active_passengers':{'$addToSet':'$payer_id'}}}
        ]

        return len(pays.aggregate(pipeline).next()['active_passengers'])

    #gives an array of users _id, registered in the given period
    def get_registered_ids(self, start, end):
        users = DB.get_users()

        pipeline = [
            {'$match':{'role':'payer', 'registerDate':{'$gt':start, '$lt':end}}},
            {'$project':{'_id':1}},
        ]
        return [id['_id'] for id in users.aggregate(pipeline)]

    #gives array of retention percent for the cohort of registered users in [start,end] period
    #start and end are instance of _Date
    def calculate_retention(self, start, end):
        start = _Date(JalaliDatetime(start))
        end = _Date(JalaliDatetime(end))
        now = JalaliDatetime.now()

        pay = Pay()
        registered_passengers = self.get_registered_ids(start.to_datetime(), end.to_datetime())

        retention = []
        retention.append(100)
        while True:
            start.month_after(1)
            end.month_after(1)

            if start.get_date() < now:
                retained = pay.how_many_have_pay(registered_passengers, start.to_datetime(), end.to_datetime())
                retention.append(float(retained) / len(registered_passengers) * 100)
            else:
                break

        return retention




def calculate_bank_revenue_per_month(amount):
    RATE = 10 # 10 percent, according to central bank
    return (30.0/365.0) * (RATE/100.0) * amount