from django.contrib import auth
from django.contrib.auth import authenticate
from django.shortcuts import render, redirect


# Create your views here.
def login(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            return redirect('/')
        else:
                username = request.POST.get('username','')
                password = request.POST.get('password', '')

                user = authenticate(username=username, password=password)
                if user is not None:
                    auth.login(request, user)
                    return redirect('/')
                else:
                    return render(request, 'authentication/login.html',{'msg':'bad username or password'})
    else:
        return render(request, 'authentication/login.html')