import os
import re

from django.conf import settings
from django.core.exceptions import MiddlewareNotUsed
from django.shortcuts import render


class RequireLoginMiddleware(object):
    """
    Middleware component that wraps the login_required decorator around
    matching URL patterns. To use, add the class to MIDDLEWARE_CLASSES and
    define LOGIN_REQUIRED_URLS and LOGIN_REQUIRED_URLS_EXCEPTIONS in your
    settings.py. For example:
    ------
    LOGIN_REQUIRED_URLS = (
        r'/topsecret/(.*)$',
    )
    LOGIN_REQUIRED_URLS_EXCEPTIONS = (
        r'/topsecret/authentication(.*)$',
        r'/topsecret/logout(.*)$',
    )
    ------
    LOGIN_REQUIRED_URLS is where you define URL patterns; each pattern must
    be a valid regex.

    LOGIN_REQUIRED_URLS_EXCEPTIONS is, conversely, where you explicitly
    define any exceptions (like authentication and logout URLs).
    """

    def __init__(self, get_response):
        if os.getenv('PARDAKHTAM_SUPPORT_DEBUG', True) != 'False':
            print '[INFO] authentication disabled'
            raise MiddlewareNotUsed()

        self.required = tuple(re.compile(url) for url in settings.LOGIN_REQUIRED_URLS)
        self.exceptions = tuple(re.compile(url) for url in settings.LOGIN_REQUIRED_URLS_EXCEPTIONS)
        self.get_response = get_response

    def __call__(self, request):
        # No need to process URLs if user already logged in
        if request.user.is_authenticated():
            return self.get_response(request)

        # An exception match should immediately return None
        for url in self.exceptions:
            if url.match(request.path):
                return self.get_response(request)

        # Requests matching a restricted URL pattern are returned
        # wrapped with the login_required decorator
        for url in self.required:
            if url.match(request.path):
                return render(request, 'authentication/login.html')
