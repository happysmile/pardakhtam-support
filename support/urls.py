from django.conf.urls import url

from support import views

urlpatterns = [
    url(r'find_user', views.find_user),
    url(r'update_user', views.update_user),
    url(r'get_pays', views.get_pays),
    url(r'get_charges', views.get_charges),
    url(r'find_passenger_by_code', views.find_passenger_by_code)
]