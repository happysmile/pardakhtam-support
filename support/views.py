# -*- coding: utf-8 -*-

import pymongo
from bson.objectid import ObjectId
from django.http import JsonResponse
from django.shortcuts import render

from db.mongo import DB


def find_user(request):
    if request.method == 'GET':
        return render(request, 'index.html')
    elif request.method == 'POST':
        driver_mobile = request.POST.get('driver_mobile', '')
        driver_code = request.POST.get('driver_code', '')
        pass_mobile = request.POST.get('pass_mobile', '')
        pass_code = request.POST.get('pass_code', '')

        if len(driver_mobile) > 0:
            return find_driver_by_mobile(driver_mobile)
        elif len(driver_code) > 0:
            return find_driver_by_code(int(driver_code))
        elif len(pass_mobile) > 0:
            return find_passenger_by_mobile(pass_mobile)
        elif len(pass_code) > 0:
            return find_passenger_by_code(int(pass_code))


def update_user(request):
    if request.method == 'POST':
        mobile = request.POST.get('mobile', '')
        property = request.POST.get('property', '')
        old_value = request.POST.get('old', '')
        correct_value = request.POST.get('correct_data', '')
        if old_value == correct_value:
            return JsonResponse({'msg': 'any thing not change'}, status=404, content_type='application/json')
        if len(mobile) > 0 and len(property) > 0:
            users = DB.get_users()

            if property == 'credit':
                # credit is int
                res = users.update_one({'mobile': mobile, property: int(old_value)},
                                       {'$set': {property: int(correct_value)}})

                # print correct_value, ',', correct_value, ',', res
            elif len(old_value) == 0:
                # property has not been set before
                res = users.update_one({'mobile': mobile}, {'$set': {property: correct_value}})
            else:
                res = users.update_one({'mobile': mobile, property: old_value}, {'$set': {property: correct_value}})

            if res.matched_count == 1 and res.modified_count == 1:
                return JsonResponse({'msg': 'updated'}, status=200, content_type='application/json')
            elif res.matched_count == 0:
                return JsonResponse({'msg': 'document needs update'}, status=400, content_type='application/json')
            else:
                return JsonResponse({'msg': 'something bad happened! escape.'}, status=403,
                                    content_type='application/json')
        else:
            return JsonResponse({'msg': 'bad input'}, status=401, content_type='application/json')
    else:
        return JsonResponse({'msg': 'just POST'}, status=402, content_type='application/json')


def find_driver_by_code(code):
    msg = ''
    status_code = 200
    driver = {}

    codes = DB.get_receiver_code()
    code = codes.find_one({'receiverID': code})
    if code is None:
        msg = 'code is none'
        status_code = 205
    else:
        users = DB.get_users()
        user = users.find_one({'_id': ObjectId(code['user'])})

        if user is None:
            msg = 'user not found'
            status_code = 203
        else:
            status_code = 200
            msg = 'succ :)'
            driver['name'] = user.get('name', '')
            driver['family'] = user.get('family', '')
            driver['credit'] = user.get('credit', '')
            driver['type'] = user.get('type', '')
            driver['line'] = user.get('line', '')
            driver['invitedBy'] = user.get('invitedBy', '')
            driver['mobile'] = user.get('mobile', '')
            driver['code'] = code.get('receiverID', '')
            driver['cardNumber'] = user.get('cardNumber', '')
            driver['sheba'] = user.get('sheba', '')
            driver['bank'] = user.get('bank', '')
            driver['sms'] = user.get('sms', '')

    out = {
        'msg': msg,
        'driver': driver
    }
    return JsonResponse(out, status=status_code, content_type='application/json')


def find_driver_by_mobile(phone_num):
    msg = ''
    status_code = 200
    driver = {}

    users = DB.get_users()
    user = users.find_one({'mobile': phone_num})
    if user is None:
        msg = 'user is none'
        status_code = 203
    else:
        codes = DB.get_receiver_code()
        code = codes.find_one({'user': ObjectId(user['_id'])})

        if code is None:
            msg = 'code not found'
            print "code not found"
            status_code = 205
        else:
            msg = 'succ :)'
            status_code = 200
            code = code['receiverID']
            driver['name'] = user.get('name', '')
            driver['family'] = user.get('family', '')
            driver['credit'] = user.get('credit', '')
            driver['type'] = user.get('type', '')
            driver['line'] = user.get('line', '')
            driver['invitedBy'] = user.get('invitedBy', '')
            driver['mobile'] = user.get('mobile', '')
            driver['cardNumber'] = user.get('cardNumber', '')
            driver['code'] = code
            driver['sheba'] = user.get('sheba', '')
            driver['bank'] = user.get('bank', '')
            driver['sms'] = user.get('sms', '')

    out = {
        'msg': msg,
        'driver': driver
    }
    return JsonResponse(out, status=status_code, content_type='application/json')


def find_passenger_by_mobile(passenger_mobile):
    msg = ''
    status_code = 200
    passenger = {}
    users = DB.get_users()
    user = users.find_one({'mobile': passenger_mobile})
    if user is None:
        msg = 'user is none'
        status_code = 204
    else:
        codes = DB.get_payer_code()
        code = codes.find_one({'user': ObjectId(user['_id'])})

        if code is None:
            msg = 'passenger not found'
            status_code = 206
            # this num is a driver
        else:
            msg = 'succ :)'
            status_code = 200
            code = code['payerID']
            passenger['credit'] = user.get('credit', '')
            passenger['mobile'] = user.get('mobile', '')
            passenger['code'] = code
    out = {
        'msg': msg,
        'passenger': passenger
    }
    return JsonResponse(out, status=status_code, content_type='application/json')


def find_passenger_by_code(code):
    msg = ''
    status_code = 200
    passenger = {}

    codes = DB.get_payer_code()
    code = codes.find_one({'payerID': code})
    if code is None:
        msg = 'code is none'
        status_code = 207
    else:
        users = DB.get_users()
        user = users.find_one({'_id': ObjectId(code['user'])})

        if user is None:
            msg = 'user not found'
            status_code = 203
        else:
            status_code = 200
            msg = 'succ :)'
            passenger['credit'] = user.get('credit', '')
            passenger['invitedBy'] = user.get('invitedBy', '')
            passenger['mobile'] = user.get('mobile', '')
            passenger['code'] = code.get('payerID', '')
    out = {
        'msg': msg,
        'passenger': passenger
    }
    return JsonResponse(out, status=status_code, content_type='application/json')


def get_pays(request):
    if request.method == 'POST':
        num = int(request.POST.get('num', '40'))
        payer_mobile = request.POST.get('passenger_mobile', '')
        receiver_mobile = request.POST.get('receiver_mobile', '')
        print "payer_mobile=" + payer_mobile
        print "receiver_mobile=" + receiver_mobile

        pays = DB.get_pays()

        if len(payer_mobile) > 0:
            pays = pays.find({'payer_mobile': payer_mobile}).limit(num).sort('date', pymongo.DESCENDING)
        elif len(receiver_mobile) > 0:
            pays = pays.find({'receiver_mobile': receiver_mobile}).limit(num).sort('date', pymongo.DESCENDING)
        else:
            return JsonResponse({'msg': 'bad input'}, status=400, content_type='application/json')
        out = {'pays': []}
        for pay in pays:
            out['pays'].append({'receiver_mobile': pay.get('receiver_mobile'),
                                'payer_mobile': pay.get('payer_mobile'),
                                'date': pay.get('date'),
                                'amount': pay.get('amount')})

        return JsonResponse(out, status=200, content_type='application/json')


def get_charges(request):
    if request.method == 'POST':
        mobile = request.POST.get('charger_mobile')
        print (mobile)
        num = int(request.POST.get('num', '10'))
    if len(mobile) > 0:
        users = DB.get_users()
        user = users.find_one({'mobile': mobile})
        if user is not None:
            charges = DB.get_charges()
            charges = charges.find({'$or': [{'chargerMobile': mobile}, {'payer_id': ObjectId(user['_id'])}]}).limit(
                num).sort('date', pymongo.DESCENDING)

            out = {'charges': []}
            for charge in charges:
                out['charges'].append({
                    'charger_mobile': user.get('mobile'),
                    'amount': charge.get('amount'),
                    'date': charge.get('date'),
                    'authority': charge.get('authority')
                    # 'refNum': charges.get()
                })
            return JsonResponse(out, status=200, content_type='application/json')
        else:
            return JsonResponse({'err': 'user not found'}, status=201, content_type='application/json')
    else:
        return JsonResponse({'err': 'mobile is empty'}, status=202, content_type='application/json')
