import os

from pymongo import MongoClient


class DB:
    db = None

    @staticmethod
    def get_db_name():
        return os.environ.get('DB_NAME', 'pardakhtam')

    @staticmethod
    def connect_to_db():
        if DB.db is None:
            # user = os.environ.get('DB_USER', 'userACC')
            # password = os.environ.get('DB_PASS', 'OxNe8NtINuY1tJi0')

            # debug
            user = os.environ.get('DB_USER', 'userUDC')
            password = os.environ.get('DB_PASS', '3PCe3kG8wQpv8trD')

            server = os.environ.get('DB_SERVER', '127.0.0.1')
            db_name = DB.get_db_name()

            if len(user) > 0:
                print 'connecting to production db'
                url = 'mongodb://%s:%s@%s/%s' % (user, password, server, db_name)
            else:
                print 'connecting to local DB'
                url = 'mongodb://%s' % (server)
            DB.db = MongoClient(url)

        return DB.db

    @staticmethod
    def get_pays():
        return DB.connect_to_db()[DB.get_db_name()]['pays']

    @staticmethod
    def get_users():
        return DB.connect_to_db()[DB.get_db_name()]['users']

    @staticmethod
    def get_receiver_code():
        return DB.connect_to_db()[DB.get_db_name()]['receiveridmaps']

    @staticmethod
    def get_payer_code():
        return DB.connect_to_db()[DB.get_db_name()]['payeridmaps']

    @staticmethod
    def get_charges():
        return DB.connect_to_db()[DB.get_db_name()]['charges']
